// show hide top menu

const menuToShow = document.querySelector('.menu-collapsed')
menuToShow.addEventListener('click', function ({target}) {
    const bar = this.querySelector('.bar')
    if(target === bar){
        this.classList.toggle('menu-expanded')
    }
})


// create map

const mapWrap = document.getElementById('map')
function initMap() {


    const map = new google.maps.Map(mapWrap, {
        center: { lat: 50.429061, lng: 30.593617 },
        zoom: 14,
        styles: [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ]
    });

    // marker
    const marker = new google.maps.Marker({
        position: { lat: 50.429061, lng: 30.593617 },
        map: map,
        title: 'EnerGym, Київ, пр. Павла Тичини, 1в, ТОЦ "Silver Breeze"',
        icon: './img/map-ico.png'
    });
}

//open google map

const gymAdress = 'https://www.google.com.ua/maps/place/EnerGym/@50.428597,30.5908457,17z/data=!3m1!4b1!4m5!3m4!1s0x40d4c57dea9b1907:0x50a7a374eb12a90f!8m2!3d50.428597!4d30.5930344?hl=ru'

mapWrap.addEventListener('click', ()=>{
    window.open(gymAdress, '_blanc')
})

//change language
const textToChange = {
    Ru: {
        "home": "Домашняя страница",
        "contacts": "Контакты",
        "business": "Бизнес",
        "site": "Сайт",
        "descr": "6 000 кв.м. фитнеса с потрясающим видом на правый берег Киева, Лавру и Днепр.",
        "training": "групповой тренажерный зал",
        "cardio": "кардио",
        "pool": "профессиональный спортивный бассейн 25х15 м с 6 дорожками для плавания и 2 детскими бассейнами",
        "kid": "детский клуб EnerKid 500 кв.м.",
        "bar": "фитнес-бар",
        "box": "Бойцовский клуб",
        "map": "проспект Павла Тичини, 1в, Київ",
        "contactsTitle": "контакты",
        "locationText": `Киев, Украина<br>Минск, Беларусь`

    },
    Eng: {
        "home": "Home",
        "contacts": "Contacts",
        "business": "Business",
        "site": "Website",
        "descr": "6 000 sq.m. of comfortable fitness with amazing view of the right bank of Kiev, the Lavra and the Dnieper.",
        "training": "fitness center group training hall",
        "cardio": "cardio theater",
        "pool": "professional sports pool 25х15 m with 6 swim lanes и 2 wading pools",
        "kid": "children’s club EnerKid 500 q.m.",
        "bar": "fitness bar",
        "box": "fight Club",
        "map": "1 Pavla Tychyny Ave, Kiev, Ukraine",
        "contactsTitle": "contacts",
        "locationText": `Kiev, Ukraine<br>Minsk, Belarus`

    }
};
const navBar = document.querySelector('.nav-list')
navBar.addEventListener('click', function ({target}) {
    let lang = target.textContent
    let arrLang = textToChange[lang]
    const elements = document.querySelectorAll("[data-translate]");
    console.log(arrLang)
    if(lang==='Ru' || lang==='Eng'){

        elements.forEach(elem => {
            const phrase = elem.dataset.translate;
            const text = arrLang[phrase];
            elem.innerHTML = text;
        })
    }

})
